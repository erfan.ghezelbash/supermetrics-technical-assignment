<?php

namespace App\Services;

use Carbon\Carbon;

class Supermetrics extends BaseService
{
    public function getToken()
    {
        $token = $this->getTokenFromDatabase();
        if ($token) {
            return $token;
        }
        try {
            $loginResponse = $this->login();
        } catch (\Exception $exception) {
            echo $exception->getMessage();
            return null;
        }
        $jsonResponse = json_decode($loginResponse);
        $token = $jsonResponse->data->sl_token;
        $expiresIn = 3600; //token expiration time
        $this->setTokenToDatabase($token, $expiresIn);
        return $token;
    }

    /**
     * @return bool|string
     * @throws \Exception
     */
    private function login()
    {
        $url = "https://api.supermetrics.com/assignment/register";
        $postDataArray = [
            'client_id' => $_ENV['CLIENT_ID'],
            'email' => $_ENV['EMAIL'],
            'name' => $_ENV['NAME']
        ];
        return curlRequest($url, 'POST', 10, $postDataArray);
    }

    /**
     * Using Database to store token (its better to save this token also in Redis)
     * @param $token
     * @param int $expiresIn
     */
    private function setTokenToDatabase($token, int $expiresIn)
    {
        $statement = $this->app->getDatabaseConnection()->prepare(
            "INSERT INTO credentials (token, created_at, expires_at)
              VALUES (:token, :created_at, :expires_at)");
        $statement->execute([
            ':token' => $token,
            ':created_at' => Carbon::now($_ENV['TIMEZONE']),
            ':expires_at' => Carbon::now($_ENV['TIMEZONE'])->addSeconds($expiresIn),
        ]);
    }

    private function getTokenFromDatabase()
    {
        $sqlQuery = "SELECT token from credentials where expires_at > :now order by expires_at desc limit 1";
        $statement = $this->app->getDatabaseConnection()->prepare($sqlQuery);
        $statement->execute([':now' => Carbon::now($_ENV['TIMEZONE'])]);
        return $statement->fetch()['token'];
    }
}