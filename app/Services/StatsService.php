<?php

namespace App\Services;

use App\Contracts\StatsRepositories;

class StatsService extends BaseService implements StatsRepositories
{
    public function averageCharLengthOfPostPerMonth(): ?array
    {
        $sqlQuery = "SELECT CONCAT(CONCAT(`year`, '-'), `month`) as period, CEIL(AVG(char_length)) AS avg FROM posts 
          GROUP BY `year`, `month`";
        $statement = $this->app->getDatabaseConnection()->prepare($sqlQuery);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function longestPostByCharacterLengthPerMonth(): ?array
    {
        $sqlQuery = "SELECT CONCAT(CONCAT(`year`, '-'), `month`) as period, id, from_name, from_id, message, 
              char_length, `type`, created_time FROM posts 
              WHERE char_length in (SELECT max(char_length) AS max_chars FROM posts GROUP BY `year`, `month`) 
              GROUP BY `year`, `month`";
        $statement = $this->app->getDatabaseConnection()->prepare($sqlQuery);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function totalPostsSplitByWeekNumber(): ?array
    {
        $sqlQuery = "SELECT `year`, `week_number`, COUNT(id) AS post_count FROM posts 
          GROUP BY `week_number`, `year` ORDER BY `year`, `week_number`";
        $statement = $this->app->getDatabaseConnection()->prepare($sqlQuery);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function averageNumberOfPostsPerUserPerMonth(): ?array
    {
        $sqlQuery = "SELECT from_id, from_name, AVG(monthly_count) AS average_monthly_posts_count FROM 
            (SELECT from_id, from_name, COUNT(*) AS `monthly_count` FROM posts 
            GROUP BY from_id, month ORDER BY from_id) AS subQuery GROUP BY from_id";
        $statement = $this->app->getDatabaseConnection()->prepare($sqlQuery);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }
}