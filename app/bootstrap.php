<?php

use App\Controllers\StatsControllers;
use Dotenv\Dotenv;

Class Bootstrap
{

    public $dbConnection;

    public function __construct()
    {
        $this->setEnvironmentVariable();
        $this->setDatabaseConnection();
    }

    public function getDatabaseConnection(): PDO
    {
        return $this->dbConnection;
    }

    private function setEnvironmentVariable(): void
    {
        $dotEnv = Dotenv::createImmutable(__DIR__ . '/..');
        $dotEnv->load();
    }

    private function setDatabaseConnection(): void
    {
        $dbHost = $_ENV['DB_HOST'];
        $dbName = $_ENV['DB_NAME'];
        $dbUsername = $_ENV['DB_USERNAME'];
        $dbPassword = $_ENV['DB_PASSWORD'];

        $dbConnection = new PDO("mysql:host=$dbHost;dbname=$dbName", $dbUsername, $dbPassword);
        $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->dbConnection = $dbConnection;
    }


    public function router()
    {
        $uri = $_SERVER['REQUEST_URI'];

        switch ($uri) {
            case '/stats/average-char-length-per-month':
                return (new StatsControllers())->averageCharacterLength();
                break;
            case '/stats/longest-post-by-char-length-per-month':
                return (new StatsControllers())->longestPostByCharacterLength();
                break;
            case '/stats/total-posts-split-by-week-number':
                return (new StatsControllers())->totalPostsSplitByWeekNumber();
                break;
            case '/stats/average-number-of-posts-per-user-per-month':
                return (new StatsControllers())->averageNumberOfPostsPerUserPerMonth();
                break;
            default:
                return json_encode([
                    'error' => '404 - PAGE NOT FOUND'
                ]);
        }
    }
}