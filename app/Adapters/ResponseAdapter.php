<?php

namespace App\Adapters;

class ResponseAdapter
{
    protected $data;
    protected $title;
    protected $meta;

    public function __construct($outputData, $dataTitle, array $meta = [])
    {
        $this->data = $outputData;
        $this->title = $dataTitle;
        $this->meta = $meta;
    }

    public function toJson()
    {
        $responseArray = [
            "meta" => $this->meta,
            "data" => [
                $this->title => $this->data
            ]
        ];
        return json_encode($responseArray);
    }
}