<?php

namespace App\Contracts;

interface StatsRepositories
{
    /**
     * @return array|null
     */
    public function averageCharLengthOfPostPerMonth(): ?array;

    /**
     * @return array|null
     */
    public function longestPostByCharacterLengthPerMonth(): ?array;

    /**
     * @return array|null
     */
    public function totalPostsSplitByWeekNumber(): ?array;

    /**
     * @return array|null
     */
    public function averageNumberOfPostsPerUserPerMonth(): ?array;
}