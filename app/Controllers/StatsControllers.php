<?php

namespace App\Controllers;

use App\Adapters\ResponseAdapter;
use App\Services\StatsService;

class StatsControllers
{
    protected $statsService;

    public function __construct()
    {
        $this->statsService = new StatsService();
    }

    public function averageCharacterLength()
    {
        $dataArray = $this->statsService->averageCharLengthOfPostPerMonth();
        return (new ResponseAdapter($dataArray, "average_char_length_of_post_per_month"))->toJson();
    }

    public function longestPostByCharacterLength()
    {
        $dataArray = $this->statsService->longestPostByCharacterLengthPerMonth();
        return (new ResponseAdapter($dataArray, "longest_posts_by_char_length_per_month"))->toJson();
    }

    public function totalPostsSplitByWeekNumber()
    {
        $dataArray = $this->statsService->totalPostsSplitByWeekNumber();
        return (new ResponseAdapter($dataArray, "total_posts_split_by_week_number"))->toJson();
    }

    public function averageNumberOfPostsPerUserPerMonth()
    {
        $dataArray = $this->statsService->averageNumberOfPostsPerUserPerMonth();
        return (new ResponseAdapter($dataArray, "average_number_of_posts_per_user_per_month"))->toJson();
    }
}