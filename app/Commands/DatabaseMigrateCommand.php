<?php

namespace App\Commands;

class DatabaseMigrateCommand extends BaseCommand
{
    public function runner()
    {
        echo "**** Database Migration Started **** \n";

        $this->credentialsTableMigration();
        $this->postsTableMigration();

        echo "===== Migration Done ===== \n";
    }

    private function credentialsTableMigration(): void
    {
        echo " Migrating credentials table \n";

        $sqlQuery = "CREATE TABLE IF NOT EXISTS credentials (
          id INT(9) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
          token VARCHAR(255) NOT NULL,
          created_at DATETIME NOT NULL,
          expires_at DATETIME NOT NULL
          )";
        $this->app->getDatabaseConnection()->exec($sqlQuery);
        echo " Migrated credentials table \n";
    }

    private function postsTableMigration(): void
    {
        echo " Migrating posts table \n";

        $sqlQuery = "CREATE TABLE IF NOT EXISTS posts (
          id VARCHAR(63) PRIMARY KEY,
          from_name VARCHAR(255) NOT NULL,
          from_id VARCHAR(255) NOT NULL,
          message TEXT,
          char_length INT(9),
          `type` VARCHAR(20),
          `year` INT(9),
          `month` INT(9),
          `day` INT(9),
          week_number INT(9),
          created_time DATETIME,
          INDEX (from_id),
          INDEX (char_length),
          INDEX (year),
          INDEX (month),
          INDEX (day),
          INDEX (week_number)
          )";
        $this->app->getDatabaseConnection()->exec($sqlQuery);
        echo " Migrated posts table \n";
    }
}