<?php

namespace App\Commands;

use App\Services\Supermetrics;
use Carbon\Carbon;

class FetchDataCommand extends BaseCommand
{
    protected $authToken;

    public function __construct()
    {
        parent::__construct();
        $supermetricsObject = new Supermetrics();
        $this->authToken = $supermetricsObject->getToken();
    }

    /**
     * @throws \Exception
     */
    public function runner(): void
    {
        $pdoObject = $this->app->getDatabaseConnection();
        $statement = $pdoObject->prepare(
            "INSERT IGNORE INTO posts (id, from_name, from_id, message, char_length, type, `year`, `month`, 
                      `day`, week_number, created_time)
              VALUES (:id, :from_name, :from_id, :message, :char_length, :type, :y, :m, :d, :week_number, :created_time)");

        for ($i = 1; $i <= 10; $i++) {
            $url = "https://api.supermetrics.com/assignment/posts?sl_token={$this->authToken}&page={$i}";
            try {
                $response = curlRequest($url);
            } catch (\Exception $exception) {
                echo $exception->getMessage();
                return;
            }
            $response = json_decode($response);
            $posts = $response->data->posts;
            $insertDataArray = [];
            foreach ($posts as $post) {
                $carbonDate = Carbon::parse($post->created_time);
                $weekNumber = ($carbonDate->month == 1 and $carbonDate->weekOfYear == 53) ? 1 : $carbonDate->weekOfYear;
                $insertDataArray[] = [
                    ':id' => $post->id,
                    ':from_name' => $post->from_name,
                    ':from_id' => $post->from_id,
                    ':message' => $post->message,
                    ':char_length' => strlen($post->message),
                    ':type' => $post->type,
                    ':y' => $carbonDate->year,
                    ':m' => $carbonDate->month,
                    ':d' => $carbonDate->day,
                    ':week_number' => $weekNumber,
                    ':created_time' => $carbonDate
                ];
            }
            try {
                $pdoObject->beginTransaction();
                foreach ($insertDataArray as $row) {
                    $statement->execute($row);
                }
                $pdoObject->commit();
            } catch (\Exception $e) {
                $pdoObject->rollback();
                throw $e;
            }
        }
    }
}
