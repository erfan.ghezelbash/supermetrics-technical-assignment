## **How to Run**

  

 - copy `.env.example` as `.env`  
 - `composer install`  
 - set  credentials and DB settings in `.env` file
 - `php run database:migrate`
 - `php run supermetrics:fetch` *(can be run multiple times or in crontab)*
 - `php -S localhost:8000 .\index.php`

**Now you can check four links here:** 

 1. http://localhost:8000/stats/average-char-length-per-month
 2. http://localhost:8000/stats/longest-post-by-char-length-per-month
 3. http://localhost:8000/stats/total-posts-split-by-week-number
 4. http://localhost:8000/stats/average-number-of-posts-per-user-per-month